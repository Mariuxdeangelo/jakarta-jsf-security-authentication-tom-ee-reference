import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Named;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Named
@ApplicationScoped
public class MainApplicationScope extends MainAbstractExample {

    private static Logger logger = LogManager.getLogger();
    @PostConstruct
    public void init() {
        super.init();
    }

    @PreDestroy
    public void destroy(){
        super.destruct(getClass().getName());
    }

}
