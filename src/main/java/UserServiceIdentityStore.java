import jakarta.enterprise.context.ApplicationScoped;
import jakarta.security.enterprise.credential.Credential;
import jakarta.security.enterprise.credential.UsernamePasswordCredential;
import jakarta.security.enterprise.identitystore.CredentialValidationResult;
import jakarta.security.enterprise.identitystore.IdentityStore;

import java.util.HashSet;

@ApplicationScoped
public class UserServiceIdentityStore implements IdentityStore {

    @Override
    public CredentialValidationResult validate(Credential credential) {
        UsernamePasswordCredential login = (UsernamePasswordCredential) credential;
        String email = login.getCaller();
        String password = login.getPasswordAsString();

        if (email.equals("test") && password.equals("test")) {
            HashSet<String> roles = new HashSet<>();
            roles.add("USER");
            return new CredentialValidationResult(
                    email, roles
            );
        } else {
            return CredentialValidationResult.INVALID_RESULT;
        }
    }

}
