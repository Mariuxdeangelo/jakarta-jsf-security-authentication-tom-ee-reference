import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Named;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;

@Named
@ViewScoped
public class MainViewScope extends MainAbstractExample implements Serializable {

    private static Logger logger = LogManager.getLogger();
    @PostConstruct
    public void init() {
        super.init();
    }

    @PreDestroy
    public void destroy(){
        super.destruct(getClass().getName());
    }

}
