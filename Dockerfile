FROM tomee:9-jre17

ADD /build/libs/tomee-jsf-1.0-SNAPSHOT.war /usr/local/tomee/webapps
ADD /tomcat-users.xml /usr/local/tomee/conf

EXPOSE 8080
CMD ["catalina.sh", "run"]